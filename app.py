from flask import Flask, jsonify, request
from flaskext.mysql import MySQL
import mysql.connector

app = Flask(__name__)

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="login1234",
    database="demo1"
)
my_cursor = mydb.cursor()


incomes = [{
    'description': 'salary', 'amount': 5000, 'name': 'ayush'
}, {
    'description': 'salary', 'amount': 15000, 'name': 'gaurav'
},
    {
        'description': 'salary', 'amount': 1000, 'name': 'abhijith'
    },
    {}
]


@app.route("/<name>")
def hello_world(name):
    return f"Hello {name}"


@app.route('/incomes')
def get_incomes():
    return jsonify(incomes)


@app.route('/income/<name>')
def filter_income(name):
    for income in incomes:
        if income.get('name') == name:
            return jsonify(income)
    return "Not Found"


@app.route('/get', methods=['GET'])
def get_income():
    return jsonify(incomes)


@app.route('/add', methods=['POST'])
def add_income():
    incomes.append(request.get_json())
    return "successfully added"


@app.route('/update/<name>', methods=['PUT'])
def update_income(name):
    for i in range(len(incomes)):
        if incomes[i].get('name') == name:
            del incomes[i]
            incomes.append(request.get_json())
            return "successfully updated"
    return "Name not found"


@app.route('/delete/<name>', methods=['DELETE'])
def delete_income(name):
    for i in range(len(incomes)):
        if incomes[i].get('name') == name:
            del incomes[i]
            return "successfully deleted"
    return "Name not found"


@app.route('/database/get')
def get():
    my_cursor.execute("SELECT * FROM Employee")
    my_result = my_cursor.fetchall()
    for i in my_result:
        name = i[0]
        age = i[1]
        print(name, age)
    return "done"


@app.route('/database/post')
def post():
    sql = "INSERT INTO Employee (name, age) VALUES (%s, %s)"
    name = input("Enter name")
    age = input("ENTER age")
    val = (name, age)
    my_cursor.execute(sql, val)
    mydb.commit()
    return "data inserted"


@app.route('/database/delete/<name>')
def delete(name):
    sql = "DELETE FROM Employee WHERE name=%s"
    my_cursor.execute(sql, name)
    mydb.commit()
    return "data deleted"


@app.route('/database/put/<name>')
def put(name):
    sql = "UPDATE Employee SET age=%s where name=%s"
    age = input("Enter age")
    n = (name, age)
    my_cursor.execute(sql, n)
    mydb.commit()
    return "data updated"


if __name__ == '__main__':
    app.run(debug=True)
